from django.contrib import admin

# Register your models here.

from .models import *

admin.site.register(Reservation)
admin.site.register(Commentaire)
admin.site.register(Favori)
admin.site.register(SignalMaison)
