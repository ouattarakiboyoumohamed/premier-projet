
from django.db import models
from utilisateur.models import Visiteur
from annonce.models import Annonce

# Create your models here.


class Reservation(models.Model):
    user = models.ForeignKey(Visiteur, verbose_name=("Visiteur"), on_delete=models.CASCADE)
    annonce = models.ForeignKey(Annonce, verbose_name=("annonce"), on_delete=models.CASCADE)
    dateReservation = models.DateField(auto_now_add=True)
    
    # def __str__(self):
    #     return self.user.username



class Commentaire(models.Model):
    user = models.ForeignKey(Visiteur, blank= True, verbose_name=("Visiteur"), on_delete=models.CASCADE)
    annonce = models.ForeignKey(Annonce, blank= True, verbose_name=("annonce"), on_delete=models.CASCADE)
    Description = models.TextField()
    dateCommentaire = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    
    
    # def __str__(self):
    #     return self.Description
    
    

class Favori(models.Model):
    user = models.ForeignKey(Visiteur, blank= True, verbose_name=("Visiteur"), on_delete=models.CASCADE)
    annonce = models.ForeignKey(Annonce, blank= True, verbose_name=("annonce"), on_delete=models.CASCADE)
    ajouteFavori = models.BooleanField(default=False)
    dateFavori = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    
    # def __str__(self):
    #     return self.user__username
    
    
class SignalMaison(models.Model):
    user = models.ForeignKey(Visiteur, blank= True, verbose_name=("Visiteur"), on_delete=models.CASCADE)
    annonce = models.ForeignKey(Annonce, blank= True, verbose_name=("annonce"), on_delete=models.CASCADE)
    AjoutSignale = models.BooleanField(default=False)
    dateSignalement = models.DateTimeField(auto_now_add=True, blank=True, null=True)