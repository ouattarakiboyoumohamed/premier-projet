from django.urls import path

from .views import SupprimerAnnonce, Supprimerfavoris, commentaire, favoris, reserver, favorisAfficher, reservationAfficher, commentaireAfficher, signalerMaison, SupprimerReserver, home, annonceAfficher


urlpatterns = [
    path('', home, name="home"),
    path('favoris/<str:pk>', favorisAfficher, name="favorisAff"),
    
    path('reserver/<str:pk>/<str:pu>', reserver, name="reserver"),
    
    path('supprimer/reservation/<str:pk>', SupprimerReserver, name="SupprimerReserver"),    
    path('supprimer/favoris/<str:pk>', Supprimerfavoris, name="SupprimerFavoris"),    
    path('supprimer/annonce/<str:pk>', SupprimerAnnonce, name="SupprimerAnnonce"),   
     
    path('favoris/<str:pk>/<str:pu>', favoris, name="favoris"),
    path('signaler/<str:pk>/<str:pu>', signalerMaison, name="signalerMaison"),
    path('commentaire/<str:pk>/<str:pu>', commentaire, name="commentaire"),
    path('annonceAfficher/<str:pk>', annonceAfficher, name="annonceAfficher"),
    path('reservationAfficher/<str:pk>', reservationAfficher, name="reservationAfficher"),
    path('commentaireAfficher/<str:pk>', commentaireAfficher, name="commentaireAfficher"),
        
]