
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.views.generic import DetailView
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from annonce.views import annonce
from Api.settings import LOGIN_REDIRECT_URL
from annonce.models import Annonce
from utilisateur.models import Visiteur
from .models import Favori, Reservation, Commentaire, SignalMaison

from django.contrib import messages



# Create your views here.
login_required
def home(request):
    return render(request, 'home.html')



# class FavorisListView(ListView):
#     model = Favori
#     template_name = 'user/favoris.html'

@login_required    
def favorisAfficher(request,pk):
    user = Visiteur.objects.get(user=pk)
    favoris = Favori.objects.filter(user=user).all()
    return render(request, 'user/favoris.html', context={'favoris':favoris})



@login_required
def annonceAfficher(request, pk):
    user = Visiteur.objects.get(user=pk)
    annonceAffiches = Annonce.objects.filter(client=user).all()
    reservationAnnonce = []
    
    for annonceAffiche in annonceAffiches:
        afficheAnnonce = {
            'id':annonceAffiche.id, 
            'nbrReservation': Reservation.objects.filter(annonce=annonceAffiche).count()
        }
        reservationAnnonce.append(afficheAnnonce)
        
    print(reservationAnnonce)

    
    return render(request, 'user/my-announces.html', context={'annonceAffiches':annonceAffiches, 'reservationAnnonce':reservationAnnonce})




@login_required
def reservationAfficher(request, pk):
    user = Visiteur.objects.get(user=pk)
    reservationAffiches = Reservation.objects.filter(user=user).all()
    return render(request, 'user/my-reservation.html', context={'reservationAffiches':reservationAffiches})


@login_required
def commentaireAfficher(request, pk):
    annonce = Annonce.objects.get(id=pk)
    commentaireAffiches = Commentaire.objects.filter(annonce=annonce).all()
    return render(request, 'user/single-announce.html', context={'commentaireAffiches':commentaireAffiches})


@login_required
def reserver(request,pk,pu):
    if pu == None : 
        return redirect('connexion')
    else:
        annonce = Annonce.objects.get(id=pk)
        user = Visiteur.objects.get(user=pu)
        rechercheReservation = Reservation.objects.filter(user=user, annonce=annonce).count()
        
        if rechercheReservation >= 1:
            messages.error(request, f"La maison {annonce.titre} a été déjà reservé")
            return redirect('annonceDetail', annonce.id) 
        
        else:
            reservation = Reservation()
            reservation.user = user
            reservation.annonce = annonce
            reservation.save()
            messages.success(request, f"La maison {annonce.titre} est reservé avec succes")
            return redirect('annonceDetail', annonce.id)    
        
    
     
@login_required
def favoris(request,pk,pu):
    annonce = Annonce.objects.get(id=pk)
    user = Visiteur.objects.get(user=pu)
    rechercheFavoris = Favori.objects.filter(user=user, annonce=annonce).count()
    
    if rechercheFavoris >= 1:
        messages.error(request, f"La maison {annonce.titre} est déjà en favoris")
        return redirect('annonceDetail', annonce.id) 
    
    else:
        favori = Favori()
        favori.user = user
        favori.annonce = annonce
        favori.ajouteFavori = True
        favori.save()
        messages.success(request, f"La maison {annonce.titre} est en favoris")
    return redirect('imgDetail', annonce.id)    



      
        
@login_required
def signalerMaison(request,pk,pu):
    annonce = Annonce.objects.get(id=pk)
    user = Visiteur.objects.get(user = pu)
    NbrSignaleMaison = SignalMaison.objects.filter(annonce=annonce).all().count()
    rechercheSignale = SignalMaison.objects.filter(user=user,annonce=annonce).count()
    
    if rechercheSignale >= 1:
        messages.success(request, f"La maison {annonce.titre} a déjà été siganlé donc apres 5 tentative, nous supprimerons cette annonce")
        return redirect('annonceDetail', annonce.id)
    
    else:
        signale = SignalMaison()
        signale.user = user
        signale.annonce = annonce
        signale.AjoutSignale = True
        signale.save()
        messages.success(request, f"La maison {annonce.titre} a été siganlé")
        
        print(NbrSignaleMaison)
        if NbrSignaleMaison >= 5:
            annonce.delete()
        else:
            pass
    return redirect('annonceDetail', annonce.id)    
    
    
     
        
@login_required
@csrf_exempt        
def commentaire(request,pk,pu):
    if pu == None : 
        return redirect('connexion')
    
    annonce = Annonce.objects.get(id=pk)
    user = Visiteur.objects.get(user=pu)
    commentaire = Commentaire()
    commentaire.user = user
    commentaire.annonce = annonce
    commentaire.Description = request.POST.get('commentaire')
    commentaire.save()
    commentaire.refresh_from_db()
    commentaireAffiche = commentaire.Description
    
    # commentaireAffiches = Commentaire.objects.filter(annonce=annonce).all()
    
    return JsonResponse({"commentaireAffiche":commentaireAffiche})



@login_required
def SupprimerReserver(request, pk):
    reservation = Reservation.objects.get(id=pk)
    user = reservation.user.user
    reservation.delete()
    messages.success(request, f"votre reservation a été supprimer avec succes")
    
    return redirect('reservationAfficher', user.id)


@login_required
def Supprimerfavoris(request, pk):
    favori = Favori.objects.get(id=pk)
    user = favori.user.user
    favori.delete()
    messages.success(request, f"votre maison a été retiré des favoris")

    return redirect('favorisAff', user.id)



@login_required
def SupprimerAnnonce(request, pk):
    annonce = Annonce.objects.get(id=pk)
    user = annonce.client.user
    annonce.delete()
    messages.success(request, f"votre maison a été supprimé avec succes")
    return redirect('annonceAfficher',user.id)

    