# Generated by Django 4.1 on 2022-08-09 12:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('utilisateur', '0002_remove_visiteur_nomprenom'),
        ('annonce', '0005_annonce_descriptionmaison'),
        ('action', '0005_favori_datefavori_alter_commentaire_datecommentaire'),
    ]

    operations = [
        migrations.CreateModel(
            name='SignalMaison',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('AjoutSignale', models.BooleanField(default=False)),
                ('dateSignalement', models.DateTimeField(auto_now_add=True, null=True)),
                ('annonce', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='annonce.annonce', verbose_name='annonce')),
                ('user', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='utilisateur.visiteur', verbose_name='Visiteur')),
            ],
        ),
    ]
