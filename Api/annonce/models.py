from django.db import models

from utilisateur.models import Visiteur

# Create your models here.

class Annonce(models.Model):
    client = models.ForeignKey(Visiteur, blank=True, verbose_name=("visiteur"), on_delete=models.CASCADE)
    titre = models.CharField(max_length=50, default="Villa de luxe")
    type = models.CharField(max_length=10)
    prix = models.IntegerField()
    superficie = models.IntegerField()
    nbrpiece = models.IntegerField()
    meuble = models.BooleanField(default=False)
    localite = models.CharField(max_length=50)
    datePublication = models.DateField(auto_now_add=True)
    imageMaison = models.ImageField(upload_to='images/', null=True, blank=True, default="house-1.jpg")
    lienMaison = models.TextField(null=True, blank=True, default="https://kuula.co/share/7lDdr?logo=1&info=1&fs=1&vr=0&zoom=1&sd=1&thumbs=1&inst=fr")
    descriptionMaison = models.TextField(blank=True, default="Lorem ipsum dolor sit amet consectetur adipisicing elit.Repudiandae sed dolore provident nemo veritatis, qui tempora errornobis, iure fugiat, vero maxime ipsa reprehenderit saepe vel tenetur ipsam omnis obcaecati!")
    
    def __str__(self):
        return self.client.user.username