from django.urls import path
from .import views

urlpatterns = [
    path('', views.annonce, name="home"),
    path('<int:pk>/', views.annonceDetail, name="annonceDetail"),
    path('img/<int:pk>/', views.imgDetail, name="imgDetail"),
    path('addAnnonce/<int:pk>', views.addAnnonce, name="addAnnonce"),
    path('updateAnnonce/<int:pk>/<int:pu>', views.update_annonce, name="updateAnnonce"),
    # path('annonceFiltrer/', views.annonceFiltrer, name="annonceFiltrer"),

]