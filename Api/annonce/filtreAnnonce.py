from dataclasses import field
import django_filters

from .models import Annonce

class AnnonceFiltre(django_filters.FilterSet):
    class Meta :
        model = Annonce
        fields = ['titre', 'prix', 'localite', 'type']