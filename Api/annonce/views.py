from multiprocessing.connection import Client
from django.shortcuts import render, redirect, get_object_or_404
from utilisateur.models import Visiteur
from django.contrib.auth.decorators import login_required
from .filtreAnnonce import AnnonceFiltre

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


from .models import Annonce
from action.models import Commentaire
from django.views.generic import ListView, DetailView

from django.contrib import messages

# Create your views here.
def annonce(request):
    pass

@login_required
def addAnnonce(request,pk):    
    
    if request.POST:  
        client = Visiteur.objects.get(user=pk)
        titre = request.POST['titre']
        type = request.POST['type']
        prix = request.POST['prix']
        superficie = request.POST['superficie']
        nbrpiece = request.POST['nbrpiece']
        meuble = request.POST['meuble']
        localite = request.POST['localite']
        descriptionMaison = request.POST['descriptionMaison']
        image = request.FILES['imageMaison']

        try:
            user_annonce = Annonce()
            user_annonce.titre=titre 
            user_annonce.client=client 
            user_annonce.type = type
            user_annonce.prix = prix
            user_annonce.superficie = superficie
            user_annonce.nbrpiece = nbrpiece
            user_annonce.localite = localite
            user_annonce.meuble = meuble
            user_annonce.imageMaison = image
            if descriptionMaison == "":
                pass
            else:
                user_annonce.descriptionMaison = descriptionMaison
            user_annonce.save()
            messages.success(request, f"votre maison {user_annonce.titre} a été enregistré avec succes")
            return redirect('annonceAfficher',client.user.id)
        except:
            print("Mal")
            messages.error(request, f"verifiez les informations de votre maison")
            return redirect('addAnnonce',client.user.id)
    return render(request, 'user/new-announce.html')


@login_required
def update_annonce(request, pk, pu):
    user_annonce = get_object_or_404(Annonce, pk=pk)
    client = Visiteur.objects.get(user_id=pu)
    if request.POST:  
        user_annonce.client = Visiteur.objects.get(user=pu)
        user_annonce.titre = request.POST['titre']
        user_annonce.type = request.POST['type']
        user_annonce.prix = request.POST['prix']
        user_annonce.superficie = request.POST['superficie']
        user_annonce.nbrpiece = request.POST['nbrpiece']
        user_annonce.meuble = request.POST['meuble']
        user_annonce.localite = request.POST['localite']
        user_annonce.descriptionMaison = request.POST['descriptionMaison']
        user_annonce.imageMaison = request.FILES['imageMaison']


        # Valider les données si nécessaire
        if user_annonce.titre and user_annonce.type and user_annonce.prix and user_annonce.superficie and user_annonce.nbrpiece and user_annonce.localite and user_annonce.meuble:
            user_annonce.save()
            messages.success(request, f"votre maison a été modifié avec succes")
            return redirect('annonceAfficher', client.user.id)
        else:
            messages.error(request, f"nous rencontrons des difficultés pour recuperer les informations")
            return redirect('updateAnnonce', user_annonce.id, client.user.id)
    
    return render(request, 'user/new-announce-update.html', {'user_annonce': user_annonce})


# def annonceFiltrer(request):
#     if request.POST:  
#         titre = request.POST['titre']
#         # type = request.POST['type']
#         localite = request.POST['localite']
        
#         if titre != "" or localite == "":
#             annonce = Annonce.objects.filter(titre=titre).all()
#             return render(request, 'user/filterannonce.html', context={'annonce': annonce})
        
#         elif titre == "" or localite != "":
#             annonce = Annonce.objects.filter(localite=localite).all()
#             return render(request, 'user/filterannonce.html', context={'annonce': annonce})
        
#         elif titre != "" or localite != "":
#             annonce = Annonce.objects.filter(titre=titre, localite=localite).all()
#             return render(request, 'user/filterannonce.html', context={'annonce': annonce})
#     else:
#         print("Pas de filter")


class AnnonceListView(ListView):
    model = Annonce
    template_name = 'user/acceuil.html'
    



def annonceList(request):
    annoncesListe = Annonce.objects.all()
    paginator = Paginator(annoncesListe, per_page=9)
    pageNum = request.GET.get('page')
    
    # page = request.GET.get('page')
    # try:
    #     annonces = paginator.page(page)
    # except PageNotAnInteger:
    #     annonces = paginator.page(1)
    # except EmptyPage:
    #         annonces = paginator.page(paginator.num_pages)
        
    annonceFiltre = AnnonceFiltre(request.GET, queryset=annoncesListe)
    annonces = annonceFiltre.qs
    
    if annonces.count() >= 9 :
        annonces = paginator.get_page(pageNum)
    
    return render(request, 'user/acceuil.html', context={'annonces':annonces, 'paginator':paginator,'pageNum':pageNum, 'annonceFiltre':annonceFiltre})


def annonceDetail(request, pk):
    try:
        annonce = Annonce.objects.get(id=pk)
        commentaireAffiches = Commentaire.objects.filter(annonce=annonce).order_by('dateCommentaire').reverse()[:5]
    except:
        print("error")
    return render(request, 'user/single-announce.html', context={'annonce':annonce, 'commentaireAffiches':commentaireAffiches})




def imgDetail(request, pk):
    try:
        annonce = Annonce.objects.get(id=pk)
    except:
        print("error")
    return render(request, 'user/visit.html', context={'annonce':annonce})
