from django.shortcuts import redirect, render
from django.contrib.auth.models import User
from .models import Visiteur
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required


from django.core.mail import EmailMessage
from django.contrib import messages

# Create your views here.
def home(request):
    return render(request, 'home.html')
    # return render(request, 'home.html')

def acceuil(request):
    return render(request, 'user/acceuil.html')
    # return render(request, 'home.html')


  
def inscription(request): 

    if request.POST:  
        user_email = request.POST['email']
        user_username = request.POST['name']
        user_password = request.POST['password']
        user_numTéléphone = request.POST['numTéléphone']
        user_lieuHabitation = request.POST['lieuHabitation']
        try:
            # if(User.objects.filter(username= user_username).count() != 0):
            try :
                user_obj = User.objects.create(username = user_email, first_name = user_username)
                user_obj.set_password(user_password)
                user_obj.save()
            except:
                messages.error(request, f"verifiez votre adresse email")

            user_visiteur = Visiteur()
            user_visiteur.user = user_obj
            user_visiteur.lieuHabitation = user_lieuHabitation
            user_visiteur.numTéléphone = user_numTéléphone
            user_visiteur.save()
            
            # email = EmailMessage('merci de contacter MAPISS', 'Inscription Validé', to=[user_email])
            # email.send()    
            
            # return render(request, 'user/login.html')
            user_auth = authenticate(username=user_email, password=user_password) 
            login(request, user_auth)
            return redirect('annonces')
            # email = EmailMessage('merci de contacter MAPISS', 'Inscription Validé', to=[user_email])
            # email.send()
            
        except:
            print("erreur in !!!")
            messages.error(request, f"verifiez bien vos informations")
        
    else:
        # return redirect('home')            
        print("erreur !!!")
    return render(request, 'user/signup.html')


def connexion(request):
    if request.POST:  
        user_email = request.POST['email']
        user_password = request.POST['password']
        try:
            user_auth = authenticate(username=user_email, password=user_password) 
            login(request, user_auth)
            # return redirect('http://127.0.0.1:8000/annonces/')
            return redirect('annonces')
        except:
            print("erreur 1 !!!")
            messages.error(request, f"verifiez bien vos informations : adresse email et le mot de passe")

    else:
        print('erreur 2 !!!')
    return render(request, 'user/login.html')
    
        
@login_required        
def deconnexion(request):
    try:
        logout(request)
        # return redirect('http://127.0.0.1:8000/annonces/')
        return redirect('home')
    except:
        print("error")
    # return redirect('home')
    return render(request, 'user/acceuil.html')
    
