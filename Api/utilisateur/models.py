from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save

# Create your models here.

class Visiteur(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    numTéléphone = models.CharField(max_length=20)
    lieuHabitation = models.CharField(max_length=50)
    dateCreation = models.DateField(auto_now_add=True)
    
    def __str__(self):
        return self.user.first_name
    
# @receiver(post_save, sender=User)
# def updatVisiteur(sender, instance, created, **kwags):
#     if created:
#         Visiteur.objects.create(user=instance)
#     instance.visiteur.save()