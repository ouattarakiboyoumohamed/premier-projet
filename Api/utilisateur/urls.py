from django.urls import path
from .import views

from annonce.views import AnnonceListView, annonceList

urlpatterns = [
    path('', views.home, name="home"),
    # path('annonces/', AnnonceListView.as_view(), name="annonces"),
    path('annonces/', annonceList, name="annonces"),
    path('inscription/', views.inscription, name="inscription"),
    path('connexion/', views.connexion, name="connexion"),
    path('deconnexion/', views.deconnexion, name="deconnexion"),
    
]